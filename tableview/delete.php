        <?php
        // Your database info
        $dbhost = 'localhost';
        $dbuser = 'root';
        $dbpass = '';
        $dbname = 'testdb';

        if (!isset($_GET['id']))
        {
            echo 'No ID was given...';
            exit;
        }

        $con = new mysqli($dbhost, $dbuser, $dbpass, $dbname);
         if ($con->connect_error) {
            die("Connection failed: " . $con->connect_error);

        } 


        if($stmt=$con->prepare("DELETE FROM users WHERE uid = ?")) 
        {

            $stmt->bind_param("s",$_GET['id']);
            $stmt->execute();
        } 
         $stmt->close();
         $con->close();


        header("Location:load_php1.php");

        ?>


